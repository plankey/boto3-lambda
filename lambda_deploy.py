from __future__ import print_function
import sys
import boto3
from botocore.exceptions import ClientError
import json
import os

#Set in our bitbucket pipeline, to run locally you will have to pass them in 
LAMBDA_FUNCTION_NAME = os.getenv('AWS_LAMBDA_FUNCTION_NAME')
LAMBDA_ROLE_ARN = os.getenv('AWS_LAMBDA_ROLE_ARN')


# Publishes new version of the AWS Lambda Function, if this fails because a lambda with this name does not
# exist, it will create a lambda with that name
def publish_new_version(artifact):
    try:
        #Used session to allow people to change profile
        # session = boto3.Session(profile_name='dev')
        session = boto3.Session()
        client = session.client('lambda', region_name='us-east-1')
    except ClientError as err:
        print("Failed to create boto3 client.\n" + str(err))
        return False
    #Try to update
    try:
        print('Trying to update the lambda')
        response = client.update_function_code(
            FunctionName=LAMBDA_FUNCTION_NAME,
            ZipFile=open(artifact, 'rb').read(),
            Publish=True
        )
        print(response)
        return response
    #Catch Client Error
    except ClientError as err:
        #Check if Client Error is due to ResourceNotFound
        if err.response['Error']['Code'] == 'ResourceNotFoundException':
            #Need to create lambda because it does not exist
            print("Lambda does not exist, we must create it")
            #Try to create the lambda
            try:
                #This dir must match what you zipped in your pipelines yml
                with open('/tmp/artifact.zip', 'rb') as f:
                    zipped_code = f.read()
                response = client.create_function(
                FunctionName=LAMBDA_FUNCTION_NAME,
                Runtime='python3.7',
                Role=LAMBDA_ROLE_ARN,
                Handler='index.py',
                Code=dict(ZipFile=zipped_code),
                Description='A Python Lambda Deployed Through Bamboo',
                Timeout=30,
                MemorySize=128,
                Publish=True,
                #Additional Parameters I am not using, you may want to 
                # VpcConfig={
                #     'SubnetIds': [
                #         'string',
                #     ],
                #     'SecurityGroupIds': [
                #         'string',
                #     ]
                # },
                # DeadLetterConfig={
                #     'TargetArn': 'string'
                # },
                # Environment={
                #     'Variables': {
                #         'string': 'string'
                #     }
                # },
                # KMSKeyArn='string',
                # TracingConfig={
                #     'Mode': 'Active'|'PassThrough'
                # },
                # Tags={
                #     'string': 'string'
                # },
                # Layers=[
                #     'string',
                # ]
            )
                print(response)
                return response
            except IOError as err:
                print("Failed to access " + artifact + ".\n" + str(err))
                return False
        #Failed to update for reason other than client exception
        else:
            print("Failed to update function code.\n" + str(err))
            return False
    except IOError as err:
        print("Failed to access " + artifact + ".\n" + str(err))
        return False

def main():
    " Your favorite wrapper's favorite wrapper "
    if not publish_new_version('/tmp/artifact.zip'):
        sys.exit(1)

if __name__ == "__main__":
    main()

# The code in this file was hacked together by Chris Plankey with love using code from the following: 
# https://alestic.com/2014/11/aws-lambda-cli/
# https://codeburst.io/aws-lambda-functions-made-easy-1fae0feeab27/
# https://bitbucket.org/awslabs/aws-lambda-bitbucket-pipelines-python/src/master/
# 
# Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file
# except in compliance with the License. A copy of the License is located at
#
#     http://aws.amazon.com/apache2.0/
#
# or in the "license" file accompanying this file. This file is distributed on an "AS IS"
# BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under the License.
# """
# A Bitbucket Builds template for deploying an application to AWS Lambda
# joshcb@amazon.com
# v1.0.0
# """