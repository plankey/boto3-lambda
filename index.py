import requests
import json

def my_handler(event, context):
    url = "http://worldclockapi.com/api/json/est/now"
    response = requests.request("GET", url)
    return json.JSONDecoder().decode(response.text)
