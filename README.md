Deploy to AWS Lambda
An example script and configuration for deploying a new version to an existing AWS Lambda function or creating a new lambda function with Bitbucket Pipelines. A small Python function is included to use a demo for trying out the included code and configuration.

Required Environment Variables
AWS_SECRET_ACCESS_KEY: Secret key for a user with the required permissions.
AWS_ACCESS_KEY_ID: Access key for a user with the required permissions.
AWS_DEFAULT_REGION: Region where the target AWS Lambda function is.
AWS_LAMBDA_FUNCTION_NAME: Name of the target AWS Lambda function.
AWS_LAMBDA_ROLE: Name of the target AWS Lambda function.


License
Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at

http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

Note: Other license terms may apply to certain, identified software files contained within or distributed with the accompanying software if such terms are included in the directory containing the accompanying software. Such other license terms will then apply in lieu of the terms of the software license above.